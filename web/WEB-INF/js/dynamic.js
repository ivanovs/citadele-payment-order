// workaround for bootstrap's bug when first click on styled radio reset it's value
$('label.btn').click(function() {
    if ($(this).find("input[type='radio']:checked").length) {
        return false;
    }
});
//
$(":input[name='pageLanguage']").change(function() {
    var lang = $(this).val();
    $(".trans").each(function() {
        if ($(this).data("lang") === lang) {
            $(this).show();
        } else {
            $(this).hide();
        }
    })
});
$(".datepicker").datepicker({
    "dateFormat": FORM_DATE_FORMAT,
    "beforeShow": function() {
        setTimeout(function(){
            $('.ui-datepicker').css('z-index', 99999);
        }, 0);
    }
});
function prettyLatSum(sum, currency) {
    var fl = prettyDecimal(sum).split(".");
    var inWords = "";
    try {
        switch (currency) {
            case "EUR": inWords = GetNumber2e(fl[0],fl[1]); break;
            case "USD": inWords = GetNumber2d(fl[0],fl[1]); break;
            case "LVL": inWords = GetNumber2l(fl[0],fl[1]); break;
        }
    } catch (e) {
        return "";
    }
    inWords = inWords.replace(/\s+/g,' ').trim();
    return inWords[0].toUpperCase() + inWords.slice(1);
}

$("#i-ben-bank").prepend('<option value="-" selected></option>');
$("#i-ben-bank").change(function() {
    var swiftCode = $(this).val();
    var bank = BANKS[swiftCode];

    // reset fields by default
    var country = "";
    var swiftCodeFieldValue = "";
    var paymentValueDate = "";
    // populate values if any bank selected
    if (typeof bank !== "undefined") {
        country = bank.country;
        swiftCodeFieldValue = swiftCode;
        paymentValueDate = PAYMENT_VALUES_DATES[swiftCode];
    }
    $("#i-ben-residence-country").val(country);
    $("#i-ben-bank-code").val(swiftCodeFieldValue);
    $("#i-pay-value-date").val(paymentValueDate);
    
    recalculateFields();
});
$("#i-pay-amount").bind("change keyup", function() {
    updateAmountInWords();
});
$(":input[name='paymentCurrency']").change(function() {
    recalculateFields();
    updateAmountInWords();
});

function recalculateFields() {
    var currency = $("#btns-payment-currency").find(":checked").val();
    var exchangeRate;
    if ("EUR" === currency) {
        exchangeRate = 1;
        $("#i-pay-exchange-rate").val("");
    } else {
        exchangeRate = EXCHANGE_RATE[currency];
        $("#i-pay-exchange-rate").val(exchangeRate);
    }
    var swiftCode = $("#i-ben-bank").val();
    var bank = BANKS[swiftCode];
    var fee = "";
    if (typeof bank !== "undefined") {
        fee = prettyDecimal(bank.fee * exchangeRate);
    }
    if (fee !== "") {
        var currencyLabel = "";
        switch (currency) {
            case "EUR": currencyLabel = "€"; break;
            case "USD": currencyLabel = "$"; break;
            case "LVL": currencyLabel = "Ls";
        }    
        fee = currencyLabel + " " + fee;
    }
    $("#i-pay-bank-fee").val(fee);
}

function updateAmountInWords() {
    var currency = $("#btns-payment-currency").find(":checked").val();
    var latsum = "";
    if ($("#i-pay-amount").val().trim() !== "") {
        var sum = prettyDecimal($("#i-pay-amount").val());
        latsum = prettyLatSum(sum, currency);
    }
    $("#i-pay-amount-words").val(latsum);
}

function prettyDecimal(amount) {
    return Number(amount).toFixed(2);
}

$("#payment-order-form").submit(function() {
    var form = $(this);
    var submitBtn = form.find(":input[type='submit']");
    var submitBtnHtml = submitBtn.html();
    // clear errors
    form.find(".js-input-error").remove();
    form.find(".form-group.has-error").removeClass("has-error");
    //
    submitBtn
        .html("Processing your order...")
        .attr("disabled","disabled");
    $.post("", $(this).serialize(), function(response) {
        if (response.errors) {
            $.each(response.errors, function(field, error) {
                var input = form.find(":input[name='"+field+"']");
                input.closest(".form-group").addClass("has-error");
                var errorBlock = "<span class=\"help-block js-input-error\">"+error+"</span>";
                if (!input.closest(".input-group").length) {
                    input.after(errorBlock);
                } else {
                    input.closest(".input-group").after(errorBlock);
                }
            });
        }
        if (response.systemError) {
            alert(response.systemError);
        }
        if (response.success) {
            alert("Thanks!");
            window.location.reload();
        }
        submitBtn
            .html(submitBtnHtml)
            .removeAttr("disabled");
    }, "json")
    return false;
})