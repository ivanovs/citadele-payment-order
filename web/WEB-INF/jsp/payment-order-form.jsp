<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="me.ivanovs.paymentorder.web.PaymentOrderModel" %>
<c:set var="dateFormatPattern" value="<%=PaymentOrderModel.DATE_FORMAT_PATTERN%>"/>
<c:set var="dateFormatPatternJS" value="<%=PaymentOrderModel.DATE_FORMAT_PATTERN_JS%>"/>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Maksājuma uzdevums</title>

        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- JQuery UI -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
        <!-- Local -->
        <link href="css/main.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <form:form modelAttribute="model" id="payment-order-form" method="POST">
            <div class="header">
                <div class="row">
                    <div class="col-md-4">
                        <img src="i/citadele-logo.jpg" width="80%"/>
                    </div>
                    <div class="col-md-6">
                        <h1>
                            <span class="trans" data-lang="lv">Maksājuma uzdevums</span>
                            <span class="trans" data-lang="en">Payment order</span>
                            <span class="trans" data-lang="ru">Платежное поручение</span>
                        </h1>
                    </div>
                    <div class="col-md-2">
                        <div class="btn-group pull-right" id="page-lang" data-toggle="buttons">
                            <label class="btn btn-default active">
                                <input type="radio" name="pageLanguage" value="lv" autocomplete="off" checked>lv
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="pageLanguage" value="en" autocomplete="off">en
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="pageLanguage" value="ru" autocomplete="off">ru
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 logo-info">
                        <p>
                            AS "Citadele banka", Reģ.nr. 40103303559<br/>
                            Republikas laukums 2A, Rīga, LV-1010, Latvija
                        </p>
                    </div>
                    <div class="col-md-5">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="i-customer-number" class="col-sm-4 control-label">
                                    <span class="trans" data-lang="lv">Klienta numurs</span>
                                    <span class="trans" data-lang="en">Customer number</span>
                                    <span class="trans" data-lang="ru">Номер клиента</span>
                                </label>
                                <div class="col-sm-8">
                                    <input class="form-control" id="i-customer-number" value="<c:out value="${model.customerNumber}"/>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="i-date" class="col-sm-4 control-label">
                                    <span class="trans" data-lang="lv">Datums</span>
                                    <span class="trans" data-lang="en">Date</span>
                                    <span class="trans" data-lang="ru">Дата</span>
                                </label>
                                <div class="col-sm-8">
                                    <fmt:formatDate var="orderDate" value="${model.orderDate}" pattern="${dateFormatPattern}"/>
                                    <input type="text" class="form-control datepicker" id="i-date"
                                        name="orderDate" value="${orderDate}" placeholder="${dateFormatPattern}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="trans" data-lang="lv">Maksātājs</span>
                        <span class="trans" data-lang="en">Remitter</span>
                        <span class="trans" data-lang="ru">Плательщик</span>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="i-rem-company-name" class="col-sm-4 control-label">
                                <span class="trans" data-lang="lv">Vārds, uzvārds / Uzņēmuma nosaukums</span>
                                <span class="trans" data-lang="en">Name, surname / Company name</span>
                                <span class="trans" data-lang="ru">Имя, фамилия / Название фирмы</span>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="i-rem-company-name" name="remitterName" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="i-rem-reg-number" class="col-sm-4 control-label">
                                <span class="trans" data-lang="lv">Personas kods / Reģistrācijas Nr.</span>
                                <span class="trans" data-lang="en">Personal ID number / Registration No</span>
                                <span class="trans" data-lang="ru">Персональный код / Регистрационный №</span>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="i-rem-reg-number" name="remitterId" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="i-rem-account" class="col-sm-4 control-label">
                                <span class="trans" data-lang="lv">Konta Nr.</span>
                                <span class="trans" data-lang="en">Account No</span>
                                <span class="trans" data-lang="ru">№ счета</span>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="i-rem-account" name="remitterAccountNumber" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="trans" data-lang="lv">Saņēmējs</span>
                        <span class="trans" data-lang="en">Beneficiary</span>
                        <span class="trans" data-lang="ru">Получатель</span>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="i-ben-company-name" class="col-sm-4 control-label">
                                <span class="trans" data-lang="lv">Vārds, uzvārds / Uzņēmuma nosaukums</span>
                                <span class="trans" data-lang="en">Name, surname / Company name</span>
                                <span class="trans" data-lang="ru">Имя, фамилия / Название фирмы</span>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="i-ben-company-name" name="beneficiaryName" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="i-ben-reg-number" class="col-sm-4 control-label">
                                <span class="trans" data-lang="lv">Personas kods / Reģistrācijas Nr.</span>
                                <span class="trans" data-lang="en">Personal ID number / Registration No</span>
                                <span class="trans" data-lang="ru">Персональный код / Регистрационный №</span>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="i-ben-reg-number" name="beneficiaryId" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="i-ben-account" class="col-sm-6 control-label">
                                        <span class="trans" data-lang="lv">Konta Nr.</span>
                                        <span class="trans" data-lang="en">Account No</span>
                                        <span class="trans" data-lang="ru">№ счета</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="i-ben-account" name="beneficiaryAccountNumber" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="i-ben-residence-country" class="col-sm-6 control-label">
                                        <span class="trans" data-lang="lv">Rezidences valsts</span>
                                        <span class="trans" data-lang="en">Residence country</span>
                                        <span class="trans" data-lang="ru">Страна резиденции</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="i-ben-residence-country" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="i-ben-bank" class="col-sm-6 control-label">
                                        <span class="trans" data-lang="lv">Saņēmējbanka</span>
                                        <span class="trans" data-lang="en">Beneficiary bank</span>
                                        <span class="trans" data-lang="ru">Банк получателя</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <form:select cssClass="form-control" id="i-ben-bank" path="beneficiaryBankCode"
                                            items="${model.banks}" itemLabel="title"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="i-ben-bank-code" class="col-sm-6 control-label">
                                        <span class="trans" data-lang="lv">Bankas kods</span>
                                        <span class="trans" data-lang="en">Bank code</span>
                                        <span class="trans" data-lang="ru">Банковский код</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="i-ben-bank-code" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="trans" data-lang="lv">Maksājuma informācija</span>
                        <span class="trans" data-lang="en">Payment information</span>
                        <span class="trans" data-lang="ru">Информация о платеже</span>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="i-pay-amount" class="col-sm-6 control-label">
                                        <span class="trans" data-lang="lv">Summa cipariem un valūta</span>
                                        <span class="trans" data-lang="en">Amount in figures and currency</span>
                                        <span class="trans" data-lang="ru">Сумма цифрами и валюта</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="i-pay-amount" name="paymentAmount" placeholder="0.00">
                                            <div class="input-group-btn" id="btns-payment-currency" data-toggle="buttons">
                                                <label class="btn btn-default active">
                                                    <input type="radio" name="paymentCurrency" value="EUR" autocomplete="off" checked>&euro;
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="paymentCurrency" value="USD" autocomplete="off">$
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="paymentCurrency" value="LVL" autocomplete="off">Ls
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="i-pay-bank-fee" class="col-sm-6 control-label">
                                        <span class="trans" data-lang="lv">Bankas komisija</span>
                                        <span class="trans" data-lang="en">Bank fee</span>
                                        <span class="trans" data-lang="ru">Комиссия банка</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="i-pay-bank-fee" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="i-pay-amount-words" class="col-sm-4 control-label">
                                <span class="trans" data-lang="lv">Summa vārdiem</span>
                                <span class="trans" data-lang="en">Amount in words</span>
                                <span class="trans" data-lang="ru">Сумма прописью</span>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="i-pay-amount-words" placeholder="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="i-pay-type" class="col-sm-6 control-label">
                                        <span class="trans" data-lang="lv">Maksājuma veids</span>
                                        <span class="trans" data-lang="en">Payment type</span>
                                        <span class="trans" data-lang="ru">Вид платежа</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <form:select cssClass="form-control" id="i-pay-type" path="paymentType"
                                            items="${model.paymentTypes}" itemLabel="title"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="i-pay-exchange-rate" class="col-sm-6 control-label">
                                        <span class="trans" data-lang="lv">Maiņas kurss</span>
                                        <span class="trans" data-lang="en">Exchange rate</span>
                                        <span class="trans" data-lang="ru">Обменный курс</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="i-pay-exchange-rate" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="i-pay-value-date" class="col-sm-6 control-label">
                                        <span class="trans" data-lang="lv">Valutēšanas datums</span>
                                        <span class="trans" data-lang="en">Value date</span>
                                        <span class="trans" data-lang="ru">Дата валютирования</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="i-pay-value-date" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="i-pay-external-code" class="col-sm-6 control-label">
                                        <span class="trans" data-lang="lv">Ārējā maksājuma kods</span>
                                        <span class="trans" data-lang="en"><small>Code of external payment</small></span>
                                        <span class="trans" data-lang="ru"><small>Код зарубежного платежа</small></span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="i-pay-external-code" name="paymentCodeExternal" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="i-pay-details" class="col-sm-4 control-label">
                                <span class="trans" data-lang="lv">Maksājuma mērķis</span>
                                <span class="trans" data-lang="en">Payment details</span>
                                <span class="trans" data-lang="ru">Назначение платежа</span>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="i-pay-details" name="paymentDetails" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <button type="submit" class="btn btn-lg center-block">
                    <span class="trans" data-lang="lv">Nosūtīt</span>
                    <span class="trans" data-lang="en">Submit</span>
                    <span class="trans" data-lang="ru">Отправить</span>
                </button>
            </form:form>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- Local -->
        <script src="js/latsum.min.js"></script>
        <script>
            var BANKS = {<%--
            --%><c:forEach items="${model.banks}" var="bank" varStatus="loop"><%--
                --%><c:if test="${loop.index!=0}">,</c:if><%--
                --%>"<c:out value="${bank.key}"/>":{<%--
                    --%>"swiftCode":"<c:out value="${bank.key}"/>",<%--
                    --%>"country":"<c:out value="${bank.value.country}"/>",<%--
                    --%>"fee":"<c:out value="${bank.value.fee}"/>"<%--
                --%>}<%--
            --%></c:forEach><%--
            --%>};
            var PAYMENT_VALUES_DATES = {<%--
                --%><c:forEach items="${model.paymentValueDates}" var="date" varStatus="loop"><%--
                    --%><c:if test="${loop.index!=0}">,</c:if><%--
                    --%>"<c:out value="${date.key}"/>":<%--
                    --%>"<fmt:formatDate value="${date.value}" pattern="${dateFormatPattern}"/>"<%--
                --%></c:forEach><%--
                --%>};
            var EXCHANGE_RATE = {
                "USD":<c:out value="${model.exchangeRateUSD}"/>,
                "LVL":<c:out value="${model.exchangeRateLVL}"/>
            };
            var FORM_DATE_FORMAT = "<c:out value="${dateFormatPatternJS}"/>";
        </script>
        <script src="js/dynamic.js"></script>
    </body>
</html>