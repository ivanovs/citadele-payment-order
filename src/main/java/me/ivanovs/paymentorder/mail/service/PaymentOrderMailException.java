package me.ivanovs.paymentorder.mail.service;

@SuppressWarnings("serial")
public class PaymentOrderMailException extends Exception {

    public PaymentOrderMailException(Throwable cause) {
        super("Exception while sending email with payment order", cause);
    }

}
