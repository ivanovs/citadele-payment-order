package me.ivanovs.paymentorder.mail.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class HyperTextMailSender {

    private static final String MESSAGE_ENCODING = "utf-8"; 
    
    @Autowired
    private JavaMailSender mailSender;

    public void sendMail(String from, String to, String subject, String body)
            throws MessagingException {
        // thread-safe
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, false,
                MESSAGE_ENCODING);

        helper.setFrom(from);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(body, true);

        mailSender.send(message);
    }
}
