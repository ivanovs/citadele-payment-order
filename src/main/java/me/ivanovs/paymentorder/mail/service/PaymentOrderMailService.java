package me.ivanovs.paymentorder.mail.service;

import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.mail.MessagingException;

import me.ivanovs.paymentorder.domain.PaymentOrder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Service;

@Service
public class PaymentOrderMailService {

    private static final SimpleDateFormat MAIL_DATE_FORMAT =
            new SimpleDateFormat("dd.MM.yy HH:mm");

    @Autowired
    @Qualifier("mailProperties") 
    private Properties mailConfiguration;

    @Autowired
    private HyperTextMailSender mailSender;

    public void sendPaymentOrder(PaymentOrder order) throws PaymentOrderMailException {

        final String mailAddressFrom = mailConfiguration.getProperty("mail.payment-order.address-from");
        final String mailAddressTo = mailConfiguration.getProperty("mail.payment-order.address-to");
        final String mailSubject = mailConfiguration.getProperty("mail.payment-order.subject");

        String mailBody = generateMailBody(order);

        try {
            mailSender.sendMail(mailAddressFrom, mailAddressTo, mailSubject, mailBody);
        } catch (MessagingException e) {
            throw new PaymentOrderMailException(e);
        } catch (MailException e) {
            throw new PaymentOrderMailException(e);
        }
    }

    private String generateMailBody(PaymentOrder order) {

        StringBuilder body = new StringBuilder();

        body.append("<html><body><ul>");
        appendPrettyField(body, "Customer number", order.getCustomerNumber());
        appendPrettyField(body, "Date", MAIL_DATE_FORMAT.format(order.getOrderDate()));
        appendPrettyField(body, "Remitter name", order.getRemitterName());
        appendPrettyField(body, "Remitter id", order.getRemitterId());
        appendPrettyField(body, "Remitter account", order.getRemitterAccountNumber());
        appendPrettyField(body, "Beneficiary name", order.getBeneficiaryName());
        appendPrettyField(body, "Beneficiary id", order.getBeneficiaryId());
        appendPrettyField(body, "Beneficiary account", order.getBeneficiaryAccountNumber());
        appendPrettyField(body, "Beneficiary bank", order.getBeneficiaryBank().getTitle());
        appendPrettyField(body, "Payment amount", order.getPaymentAmount() + order.getPaymentCurrency());
        appendPrettyField(body, "Payment type", order.getPaymentType().toString());
        appendPrettyField(body, "Payment value date", MAIL_DATE_FORMAT.format(order.getPaymentValueDate()));
        appendPrettyField(body, "Code of external payment", order.getPaymentCodeExternal());
        appendPrettyField(body, "Payment details", order.getPaymentDetails());
        body.append("</ul></body></html>");

        return body.toString();
    }

    private void appendPrettyField(StringBuilder sb, String fieldName, String fieldValue) {
        sb.append("<li><strong>").append(fieldName).append("</strong>: ")
            .append(fieldValue).append("</li>");
    }
}
