package me.ivanovs.paymentorder.domain;

public enum PaymentType {
    DEFAULT, URGENT, EXPRESS;

    private String title;

    PaymentType() {
        this.title = toString().charAt(0) + 
                toString().substring(1).toLowerCase();
    }

    public String getTitle() {
        return title;
    }
}
