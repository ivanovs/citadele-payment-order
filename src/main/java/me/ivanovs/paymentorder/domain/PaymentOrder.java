package me.ivanovs.paymentorder.domain;

import java.math.BigDecimal;
import java.util.Date;

public class PaymentOrder {

    private String customerNumber;
    private Date orderDate;
    private String remitterName;
    private String remitterId;
    private String remitterAccountNumber;
    private String beneficiaryName;
    private String beneficiaryId;
    private String beneficiaryAccountNumber;
    private Bank beneficiaryBank;

    private BigDecimal paymentAmount;
    private String paymentCurrency;
    private PaymentType paymentType;
    private Date paymentValueDate;
    private String paymentCodeExternal;
    private String paymentDetails;

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getRemitterName() {
        return remitterName;
    }

    public void setRemitterName(String remitterName) {
        this.remitterName = remitterName;
    }

    public String getRemitterId() {
        return remitterId;
    }

    public void setRemitterId(String remitterId) {
        this.remitterId = remitterId;
    }

    public String getRemitterAccountNumber() {
        return remitterAccountNumber;
    }

    public void setRemitterAccountNumber(String remitterAccountNumber) {
        this.remitterAccountNumber = remitterAccountNumber;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryId() {
        return beneficiaryId;
    }

    public void setBeneficiaryId(String beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }

    public String getBeneficiaryAccountNumber() {
        return beneficiaryAccountNumber;
    }

    public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
        this.beneficiaryAccountNumber = beneficiaryAccountNumber;
    }

    public Bank getBeneficiaryBank() {
        return beneficiaryBank;
    }

    public void setBeneficiaryBank(Bank beneficiaryBank) {
        this.beneficiaryBank = beneficiaryBank;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Date getPaymentValueDate() {
        return paymentValueDate;
    }

    public void setPaymentValueDate(Date paymentValueDate) {
        this.paymentValueDate = paymentValueDate;
    }

    public String getPaymentCodeExternal() {
        return paymentCodeExternal;
    }

    public void setPaymentCodeExternal(String paymentCodeExternal) {
        this.paymentCodeExternal = paymentCodeExternal;
    }

    public String getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(String paymentDetails) {
        this.paymentDetails = paymentDetails;
    }
}
