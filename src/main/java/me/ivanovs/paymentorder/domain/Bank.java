package me.ivanovs.paymentorder.domain;

import java.math.BigDecimal;

public class Bank {

    private static final int COUNTRY_CODE_CHAR_FROM = 4;
    private static final int COUNTRY_CODE_CHAR_TO = 6;

    private String title;
    private String swiftCode;
    private BigDecimal fee;
    private String country;

    public Bank(String title, String swiftCode, BigDecimal fee) {
        this.title = title;
        this.swiftCode = swiftCode;
        this.fee = fee;
        this.country = swiftCode.substring(COUNTRY_CODE_CHAR_FROM, COUNTRY_CODE_CHAR_TO);
    }

    public String getTitle() {
        return title;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public String getCountry() {
        return country;
    }
}
