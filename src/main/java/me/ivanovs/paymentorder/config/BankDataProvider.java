package me.ivanovs.paymentorder.config;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import me.ivanovs.paymentorder.domain.Bank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BankDataProvider {

    @Autowired
    @Qualifier("bankDataProperties") 
    private Properties bankData;

    private Map<String, Bank> banks = new TreeMap<String, Bank>(new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            return o1.compareTo(o2);
        }
    });

    @PostConstruct
    public void init() {
        int i = 1;
        while (bankData.getProperty("bank.title."+i) != null) {
            String title = bankData.getProperty("bank.title."+i);
            String swiftCode = bankData.getProperty("bank.swift-code."+i);
            BigDecimal fee = new BigDecimal(bankData.getProperty("bank.fee."+i));
            
            Bank bank = new Bank(title, swiftCode, fee);
            banks.put(swiftCode, bank);
            ++i;
        }
    }

    public Map<String, Bank> getBanks() {
        return banks;
    }
}
