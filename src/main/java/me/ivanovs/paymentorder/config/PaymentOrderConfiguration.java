package me.ivanovs.paymentorder.config;

import java.math.BigDecimal;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class PaymentOrderConfiguration {

    @Autowired
    @Qualifier("paymentOrderProperties") 
    private Properties orderConfiguration;

    private BigDecimal exchangeRateUSD;
    private BigDecimal exchangeRateLVL;
    
    private Integer paymentValueDelayHoursOwnBank;
    private Integer paymentValueDelayHoursDomesticBank;
    private Integer paymentValueDelayHoursForeignBank;

    @PostConstruct
    public void init() {
        exchangeRateUSD = new BigDecimal(orderConfiguration.getProperty("exchange-rate.usd"));
        exchangeRateLVL = new BigDecimal(orderConfiguration.getProperty("exchange-rate.lvl"));

        paymentValueDelayHoursOwnBank = Integer.valueOf(orderConfiguration.getProperty("payment.value-delay-hours.own-bank"));
        paymentValueDelayHoursDomesticBank = Integer.valueOf(orderConfiguration.getProperty("payment.value-delay-hours.domestic-bank"));
        paymentValueDelayHoursForeignBank = Integer.valueOf(orderConfiguration.getProperty("payment.value-delay-hours.foreign-bank"));
    }

    public BigDecimal getExchangeRateUSD() {
        return exchangeRateUSD;
    }

    public BigDecimal getExchangeRateLVL() {
        return exchangeRateLVL;
    }

    public Integer getPaymentValueDelayHoursOwnBank() {
        return paymentValueDelayHoursOwnBank;
    }

    public Integer getPaymentValueDelayHoursDomesticBank() {
        return paymentValueDelayHoursDomesticBank;
    }

    public Integer getPaymentValueDelayHoursForeignBank() {
        return paymentValueDelayHoursForeignBank;
    }
}
