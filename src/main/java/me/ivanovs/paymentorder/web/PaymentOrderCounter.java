package me.ivanovs.paymentorder.web;

import org.springframework.stereotype.Component;

// simplest unique id counter - just for demonstration purposes
@Component
public class PaymentOrderCounter {

    private final String ORDER_ID_FORMAT = "%08d";

    private int count = 0;

    public String getNextId() {
        return String.format(ORDER_ID_FORMAT, count + 1);
    }

    synchronized public String incrementAndGetId() {
        return String.format(ORDER_ID_FORMAT, ++count);
    }
}
