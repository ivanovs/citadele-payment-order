package me.ivanovs.paymentorder.web;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import me.ivanovs.paymentorder.config.PaymentOrderConfiguration;
import me.ivanovs.paymentorder.domain.Bank;

@Component
public class PaymentOrderCalculator {

    @Autowired
    private PaymentOrderConfiguration configuration;

    private static final String HOME_SWIFT_CODE = "PARXLV22";
    private static final String HOME_COUNTRY = "LV";
    
    // example for calculation of payment value date - depends on target bank's residence;
    // we could check a type of payment as well...
    // 
    public Date predictPaymentValueDate(Date orderDate, Bank bankTo) {
        int delayInHours;
        if (HOME_SWIFT_CODE.equals(bankTo.getSwiftCode())) {
            delayInHours = configuration.getPaymentValueDelayHoursOwnBank();
        } else if (HOME_COUNTRY.equals(bankTo.getCountry())) {
            delayInHours = configuration.getPaymentValueDelayHoursDomesticBank();
        } else {
            delayInHours = configuration.getPaymentValueDelayHoursForeignBank();
        }
        DateTime dateTime = new DateTime(orderDate).plusHours(delayInHours);
        // skip weekend
        while (dateTime.getDayOfWeek() >= DateTimeConstants.SATURDAY) {
            dateTime = dateTime.plusDays(1);
        }
        return dateTime.toDate();
    }
}
