package me.ivanovs.paymentorder.web.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import me.ivanovs.paymentorder.config.BankDataProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SwiftCodeValidator implements ConstraintValidator<SwiftCode, String> {

    @Autowired
    private BankDataProvider bankDataProvider;

    @Override
    public void initialize(SwiftCode paramA) { }
 
    @Override
    public boolean isValid(String swiftCode, ConstraintValidatorContext ctx) {
        if (swiftCode == null) {
            return false;
        }
        return bankDataProvider.getBanks().get(swiftCode) != null;
    }
}
