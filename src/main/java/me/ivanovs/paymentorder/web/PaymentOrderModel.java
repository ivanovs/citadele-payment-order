package me.ivanovs.paymentorder.web;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import me.ivanovs.paymentorder.domain.Bank;
import me.ivanovs.paymentorder.domain.PaymentType;
import me.ivanovs.paymentorder.web.validation.SwiftCode;

public class PaymentOrderModel {

    public static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";
    public static final String DATE_FORMAT_PATTERN_JS = "dd.mm.yy";

    @DateTimeFormat(pattern=DATE_FORMAT_PATTERN)
    @NotNull
    private Date orderDate;

    @NotEmpty
    private String remitterName;

    @NotEmpty
    private String remitterId;

    @NotEmpty
    // we could also add constraints for IBAN's validation depending on the appropriate bank
    // (see @SwiftCode as example)
    private String remitterAccountNumber;

    @NotEmpty
    private String beneficiaryName;

    @NotEmpty
    private String beneficiaryId;

    @NotEmpty
    private String beneficiaryAccountNumber;

    @NotEmpty
    @SwiftCode
    private String beneficiaryBankCode;

    @NumberFormat(style=Style.CURRENCY)
    @NotNull
    @DecimalMin("0.05")
    private BigDecimal paymentAmount;

    @NotEmpty
    @Pattern(regexp="EUR|USD|LVL")
    private String paymentCurrency;

    @NotNull
    private PaymentType paymentType;

    private String paymentCodeExternal;

    @NotEmpty
    private String paymentDetails;
    
    // fields unchangeable by user
    private String customerNumber;
    private String residenceCountry; // depends on a bank only
    private BigDecimal paymentBankFee;
    private String paymentAmountInWords;
    private BigDecimal paymentExchangeRate;

    // unchangeable by user
    private Date paymentValueDate;
    
    // configuration fields
    private Map<String, Bank> banks;
    private Map<String, Date> paymentValueDates;
    private BigDecimal exchangeRateUSD;
    private BigDecimal exchangeRateLVL;
    private PaymentType[] paymentTypes = PaymentType.values();

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getRemitterName() {
        return remitterName;
    }

    public void setRemitterName(String remitterName) {
        this.remitterName = remitterName;
    }

    public String getRemitterId() {
        return remitterId;
    }

    public void setRemitterId(String remitterId) {
        this.remitterId = remitterId;
    }

    public String getRemitterAccountNumber() {
        return remitterAccountNumber;
    }

    public void setRemitterAccountNumber(String remitterAccountNumber) {
        this.remitterAccountNumber = remitterAccountNumber;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryId() {
        return beneficiaryId;
    }

    public void setBeneficiaryId(String beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }

    public String getBeneficiaryAccountNumber() {
        return beneficiaryAccountNumber;
    }

    public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
        this.beneficiaryAccountNumber = beneficiaryAccountNumber;
    }

    public String getResidenceCountry() {
        return residenceCountry;
    }

    public void setResidenceCountry(String residenceCountry) {
        this.residenceCountry = residenceCountry;
    }

    public String getBeneficiaryBankCode() {
        return beneficiaryBankCode;
    }

    public void setBeneficiaryBankCode(String beneficiaryBankCode) {
        this.beneficiaryBankCode = beneficiaryBankCode;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    public BigDecimal getPaymentBankFee() {
        return paymentBankFee;
    }

    public void setPaymentBankFee(BigDecimal paymentBankFee) {
        this.paymentBankFee = paymentBankFee;
    }

    public String getPaymentAmountInWords() {
        return paymentAmountInWords;
    }

    public void setPaymentAmountInWords(String paymentAmountInWords) {
        this.paymentAmountInWords = paymentAmountInWords;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public BigDecimal getPaymentExchangeRate() {
        return paymentExchangeRate;
    }

    public void setPaymentExchangeRate(BigDecimal paymentExchangeRate) {
        this.paymentExchangeRate = paymentExchangeRate;
    }

    public Date getPaymentValueDate() {
        return paymentValueDate;
    }

    public void setPaymentValueDate(Date paymentValueDate) {
        this.paymentValueDate = paymentValueDate;
    }

    public String getPaymentCodeExternal() {
        return paymentCodeExternal;
    }

    public void setPaymentCodeExternal(String paymentCodeExternal) {
        this.paymentCodeExternal = paymentCodeExternal;
    }

    public String getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(String paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public Map<String, Bank> getBanks() {
        return banks;
    }

    public void setBanks(Map<String, Bank> banks) {
        this.banks = banks;
    }

    public Map<String, Date> getPaymentValueDates() {
        return paymentValueDates;
    }

    public void setPaymentValueDates(Map<String, Date> paymentValueDates) {
        this.paymentValueDates = paymentValueDates;
    }

    public BigDecimal getExchangeRateUSD() {
        return exchangeRateUSD;
    }

    public void setExchangeRateUSD(BigDecimal exchangeRateUSD) {
        this.exchangeRateUSD = exchangeRateUSD;
    }

    public BigDecimal getExchangeRateLVL() {
        return exchangeRateLVL;
    }

    public void setExchangeRateLVL(BigDecimal exchangeRateLVL) {
        this.exchangeRateLVL = exchangeRateLVL;
    }

    public PaymentType[] getPaymentTypes() {
        return paymentTypes;
    }
}
