package me.ivanovs.paymentorder.web;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import me.ivanovs.paymentorder.config.BankDataProvider;
import me.ivanovs.paymentorder.config.PaymentOrderConfiguration;
import me.ivanovs.paymentorder.domain.Bank;
import me.ivanovs.paymentorder.domain.PaymentOrder;
import me.ivanovs.paymentorder.mail.service.PaymentOrderMailException;
import me.ivanovs.paymentorder.mail.service.PaymentOrderMailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

@Controller
public class PaymentOrderController {

    private static final String ORDER_FORM_VIEW = "payment-order-form";

    @Autowired
    private PaymentOrderConfiguration orderConfiguration;

    @Autowired
    private BankDataProvider bankDataProvider;

    @Autowired
    private PaymentOrderCounter orderCounter;

    @Autowired
    private PaymentOrderCalculator orderCalculator;

    @Autowired
    private PaymentOrderMailService mailService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView getPaymentOrderForm() {
        PaymentOrderModel order = new PaymentOrderModel();

        Date now = new Date();
        order.setOrderDate(now);

        order.setCustomerNumber(orderCounter.getNextId());

        populateModel(order, now);
        return new ModelAndView(ORDER_FORM_VIEW, "model", order);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST,
            produces = "application/json; charset=utf-8")
    @ResponseBody
    public String submitPaymentOrderForm(@Valid PaymentOrderModel order, BindingResult bindingResult) {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        Gson responseGson = new Gson();
        boolean hasErrors = false;
        
        Map<String, String> validationErrors = getValidationErrors(bindingResult);
        if (validationErrors != null) {
            responseJSON.put("errors", validationErrors);
            hasErrors = true;
        }

        if (!hasErrors) {
            // create new payment order
            PaymentOrder paymentOrder = createPaymentOrder(order);
            try {
                // send email
                mailService.sendPaymentOrder(paymentOrder);
            } catch (PaymentOrderMailException e) {
                responseJSON.put("systemError", e.getMessage());
                hasErrors = true;
            }
        }

        if (!hasErrors) {
            responseJSON.put("success", "true");
        }
        return responseGson.toJson(responseJSON);
    }

    private void populateModel(PaymentOrderModel order, Date now) {
        // list of all banks
        Map<String, Bank> banks = bankDataProvider.getBanks();
        order.setBanks(banks);

        // payment values delays
        Map<String, Date> paymentValueDates = new HashMap<String, Date>();
        for (Bank bank : banks.values()) {
            Date paymentValueDate = orderCalculator.predictPaymentValueDate(now, bank);
            paymentValueDates.put(bank.getSwiftCode(), paymentValueDate);
        }
        order.setPaymentValueDates(paymentValueDates);

        // exchange rates
        order.setExchangeRateUSD(orderConfiguration.getExchangeRateUSD());
        order.setExchangeRateLVL(orderConfiguration.getExchangeRateLVL());
    }

    private Map<String, String> getValidationErrors(BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            Map<String, String> errorMap = new HashMap<String, String>();
            for (FieldError err : bindingResult.getFieldErrors()) {
                errorMap.put(err.getField(), err.getCode());
            }
            return errorMap;
        }
        return null;
    }

    private PaymentOrder createPaymentOrder(PaymentOrderModel model) {
        PaymentOrder order = new PaymentOrder();

        order.setCustomerNumber(orderCounter.incrementAndGetId());

        order.setOrderDate(model.getOrderDate());
        order.setRemitterName(model.getRemitterName());
        order.setRemitterId(model.getRemitterId());
        order.setRemitterAccountNumber(model.getRemitterAccountNumber());
        order.setBeneficiaryName(model.getBeneficiaryName());
        order.setBeneficiaryId(model.getBeneficiaryId());
        order.setBeneficiaryAccountNumber(model.getBeneficiaryAccountNumber());

        String swiftCode = model.getBeneficiaryBankCode();
        Bank bank = bankDataProvider.getBanks().get(swiftCode);
        order.setBeneficiaryBank(bank);
        order.setPaymentAmount(model.getPaymentAmount());
        order.setPaymentCurrency(model.getPaymentCurrency());

        order.setPaymentType(model.getPaymentType());
        // recalculate value date
        Date valueDate = orderCalculator.predictPaymentValueDate(
                model.getOrderDate(), bank);
        order.setPaymentValueDate(valueDate);
        order.setPaymentCodeExternal(model.getPaymentCodeExternal());
        order.setPaymentDetails(model.getPaymentDetails());

        return order;
    }
}
